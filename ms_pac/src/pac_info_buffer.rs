use ms_dtyp::ULONG;

use crate::{
    KERB_VALIDATION_INFO, PAC_CLIENT_CLAIMS_INFO, PAC_CLIENT_INFO,
    PAC_CREDENTIAL_INFO, PAC_DEVICE_CLAIMS_INFO, PAC_DEVICE_INFO,
    PAC_SIGNATURE_DATA, S4U_DELEGATION_INFO, UPN_DNS_INFO,
};

pub const PAC_LOGON_INFO: ULONG = 0x00000001;
pub const PAC_CREDENTIALS_INFO: ULONG = 0x00000002;
pub const PAC_SERVER_CHECKSUM: ULONG = 0x00000006;
pub const PAC_PRIVSVR_CHECKSUM: ULONG = 0x00000007;
pub const PAC_CLIENT_INFO_TYPE: ULONG = 0x0000000a;
pub const PAC_DELEGATION_INFO: ULONG = 0x0000000b;
pub const PAC_UPN_DNS_INFO: ULONG = 0x0000000c;
pub const PAC_CLIENT_CLAIMS_INFO_TYPE: ULONG = 0x0000000d;
pub const PAC_DEVICE_INFO_TYPE: ULONG = 0x0000000e;
pub const PAC_DEVICE_CLAIMS_INFO_TYPE: ULONG = 0x0000000f;

/// ```c
/// typedef struct _PAC_INFO_BUFFER {
///   ULONG ulType;
///   ULONG cbBufferSize;
///   ULONG64 Offset;
/// } PAC_INFO_BUFFER, *PPAC_INFO_BUFFER;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub enum PAC_INFO_BUFFER {
    LOGON_INFO(KERB_VALIDATION_INFO),
    CREDENTIALS_INFO(PAC_CREDENTIAL_INFO),
    SERVER_CHECKSUM(PAC_SIGNATURE_DATA),
    PRIVSRV_CHECKSUM(PAC_SIGNATURE_DATA),
    CLIENT_INFO(PAC_CLIENT_INFO),
    DELEGATION_INFO(S4U_DELEGATION_INFO),
    UPN_DNS_INFO(UPN_DNS_INFO),
    CLIENT_CLAIMS_INFO(PAC_CLIENT_CLAIMS_INFO),
    DEVICE_INFO(PAC_DEVICE_INFO),
    DEVICE_CLAIMS_INFO(PAC_DEVICE_CLAIMS_INFO),
    Raw(ULONG, Vec<u8>)
}


impl PAC_INFO_BUFFER {

    pub fn ulType(&self) -> ULONG {
        match self {
            Self::LOGON_INFO(_) => PAC_LOGON_INFO,
            Self::CREDENTIALS_INFO(_) => PAC_CREDENTIALS_INFO,
            Self::SERVER_CHECKSUM(_) => PAC_SERVER_CHECKSUM,
            Self::PRIVSRV_CHECKSUM(_) => PAC_PRIVSVR_CHECKSUM,
            Self::CLIENT_INFO(_) => PAC_CLIENT_INFO_TYPE,
            Self::DELEGATION_INFO(_) => PAC_DELEGATION_INFO,
            Self::UPN_DNS_INFO(_) => PAC_UPN_DNS_INFO,
            Self::CLIENT_CLAIMS_INFO(_) => PAC_CLIENT_CLAIMS_INFO_TYPE,
            Self::DEVICE_INFO(_) => PAC_DEVICE_INFO_TYPE,
            Self::DEVICE_CLAIMS_INFO(_) => PAC_DEVICE_CLAIMS_INFO_TYPE,
            Self::Raw(ulType, _) => *ulType,
        }
    }

    pub fn build_data(&self) -> Vec<u8> {
        match self {
            Self::LOGON_INFO(kerb_info) => kerb_info.build(),
            Self::CLIENT_INFO(client_info) => client_info.build(),
            Self::SERVER_CHECKSUM(sign_data) => sign_data.clone().build(),
            Self::PRIVSRV_CHECKSUM(sign_data) => sign_data.clone().build(),
            _ => unimplemented!("No implemented build_data for this type of buffer info")
        }
    }
}
