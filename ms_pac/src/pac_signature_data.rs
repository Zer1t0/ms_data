/// As specified in [RFC4120] and [RFC4757] section 4.
/// Signature size is 16 bytes. Decimal value is -138.
pub const KERB_CHECKSUM_HMAC_MD5: i32 = -138;
pub const KERB_CHECKSUM_HMAC_MD5_SIGN_SIZE: usize = 16;

/// As specified in [RFC3962] section 7.
/// Signature size is 12 bytes. Decimal value is 15.
pub const HMAC_SHA1_96_AES128: i32 = 15;

/// As specified in [RFC3962] section 7.
/// Signature size is 12 bytes. Decimal value is 16.
pub const HMAC_SHA1_96_AES256: i32 = 16;

pub const HMAC_SHA1_96_AES_SIGN_SIZE: usize = 12;

#[derive(Clone, Debug, PartialEq)]
pub struct PAC_SIGNATURE_DATA {
    pub SignatureType: i32,
    pub Signature: Vec<u8>,
    pub RODCIdentifier: Option<u16>,
}

impl PAC_SIGNATURE_DATA {
    pub fn new_empty(SignatureType: i32) -> Self {
        let zero_vec = match SignatureType {
            KERB_CHECKSUM_HMAC_MD5 => vec![0; KERB_CHECKSUM_HMAC_MD5_SIGN_SIZE],
            _ => vec![0; HMAC_SHA1_96_AES_SIGN_SIZE],
        };

        return Self {
            SignatureType,
            Signature: zero_vec,
            RODCIdentifier: None,
        };
    }

    pub fn build(mut self) -> Vec<u8> {
        let mut data = Vec::new();
        data.append(&mut self.SignatureType.to_le_bytes().to_vec());
        data.append(&mut self.Signature);
        return data;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_build() {
        let pac_sig = PAC_SIGNATURE_DATA {
            SignatureType: KERB_CHECKSUM_HMAC_MD5,
            Signature: vec![0; 16],
            RODCIdentifier: None,
        };
        assert_eq!(
            vec![
                0x76, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
            ],
            pac_sig.build()
        )
    }

    #[test]
    fn test_new_empty() {
        let pac_sig = PAC_SIGNATURE_DATA {
            SignatureType: KERB_CHECKSUM_HMAC_MD5,
            Signature: vec![0; 16],
            RODCIdentifier: None,
        };
        assert_eq!(pac_sig, PAC_SIGNATURE_DATA::new_empty(KERB_CHECKSUM_HMAC_MD5));

        let pac_sig = PAC_SIGNATURE_DATA {
            SignatureType: HMAC_SHA1_96_AES128,
            Signature: vec![0; 12],
            RODCIdentifier: None,
        };
        assert_eq!(pac_sig, PAC_SIGNATURE_DATA::new_empty(HMAC_SHA1_96_AES128));

        let pac_sig = PAC_SIGNATURE_DATA {
            SignatureType: HMAC_SHA1_96_AES256,
            Signature: vec![0; 12],
            RODCIdentifier: None,
        };
        assert_eq!(pac_sig, PAC_SIGNATURE_DATA::new_empty(HMAC_SHA1_96_AES256));
    }
}
