use ms_dtyp::RPC_SID;

/// ```c
/// typedef struct _RPC_SID *PISID;
/// ```
pub type PISID = RPC_SID;
