use ms_dtyp::CHAR;

/// ```c
/// typedef struct _CYPHER_BLOCK {
///   CHAR data[8];
/// }CYPHER_BLOCK;
/// ```
#[derive(Clone, Default, Debug, PartialEq)]
pub struct CYPHER_BLOCK {
    pub data: [CHAR; 8],
}

impl CYPHER_BLOCK {
    pub fn to_bytes(&self) -> [u8; 8] {
        return [
            self.data[0] as u8,
            self.data[1] as u8,
            self.data[2] as u8,
            self.data[3] as u8,
            self.data[4] as u8,
            self.data[5] as u8,
            self.data[6] as u8,
            self.data[7] as u8,
        ];
    }
}

/// ```c
/// typedef struct _USER_SESSION_KEY {
///   CYPHER_BLOCK data[2];
/// }USER_SESSION_KEY;
/// ```
#[derive(Clone, Default, Debug, PartialEq)]
pub struct USER_SESSION_KEY {
    pub data: [CYPHER_BLOCK; 2],
}

impl USER_SESSION_KEY {
    pub fn to_bytes(&self) -> [u8; 16] {
        let low_bytes = self.data[0].to_bytes();
        let high_bytes = self.data[1].to_bytes();

        return [
            low_bytes[0],
            low_bytes[1],
            low_bytes[2],
            low_bytes[3],
            low_bytes[4],
            low_bytes[5],
            low_bytes[6],
            low_bytes[7],
            high_bytes[0],
            high_bytes[1],
            high_bytes[2],
            high_bytes[3],
            high_bytes[4],
            high_bytes[5],
            high_bytes[6],
            high_bytes[7],
        ];
    }
}
