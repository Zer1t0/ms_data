use ms_dtyp::{ULONG, USHORT};

/// ```c
/// typedef struct _UPN_DNS_INFO {
///   USHORT UpnLength;
///   USHORT UpnOffset;
///   USHORT DnsDomainNameLength;
///   USHORT DnsDomainNameOffset;
///   ULONG Flags;
/// } UPN_DNS_INFO, *PUPN_DNS_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct UPN_DNS_INFO {
    pub UpnLength: USHORT,
    pub UpnOffset: USHORT,
    pub DnsDomainNameLength: USHORT,
    pub DnsDomainNameOffset: USHORT,
    pub Flags: ULONG,
}
