use ms_dtyp::ULONG;
use crate::PISID;

/// typedef struct _GROUP_MEMBERSHIP {
///   ULONG RelativeId;
///   ULONG Attributes;
/// } GROUP_MEMBERSHIP,
///  *PGROUP_MEMBERSHIP;
#[derive(Clone, Default, Debug, PartialEq)]
pub struct GROUP_MEMBERSHIP {
    pub RelativeId: ULONG,
    pub Attributes: ULONG,
}

impl GROUP_MEMBERSHIP {
    pub fn new(RelativeId: ULONG, Attributes: ULONG) -> Self {
        return Self {
            RelativeId,
            Attributes
        }
    }

    pub fn to_le_bytes(&self) -> [u8; 8] {
        let rel_bytes = self.RelativeId.to_le_bytes();
        let attr_bytes = self.Attributes.to_le_bytes();

        return [
            rel_bytes[0],
            rel_bytes[1],
            rel_bytes[2],
            rel_bytes[3],
            attr_bytes[0],
            attr_bytes[1],
            attr_bytes[2],
            attr_bytes[3],
        ];
    }
}

/// typedef struct DOMAIN_GROUP_MEMBERSHIP {
///   PISID DomainId;
///   ULONG GroupCount;
///   [size_is(GroupCount)] PGROUP_MEMBERSHIP GroupIds;
/// } DOMAIN_GROUP_MEMBERSHIP,
///  *PDOMAIN_GROUP_MEMBERSHIP;
#[derive(Clone, Default, Debug, PartialEq)]
pub struct DOMAIN_GROUP_MEMBERSHIP {
    pub DomainId: PISID,
    pub GroupCount: ULONG,
    pub GroupIds: Vec<GROUP_MEMBERSHIP>,
}
