use ms_dtyp::{RPC_UNICODE_STRING, ULONG};

/// ```c
/// typedef struct _S4U_DELEGATION_INFO {
///   RPC_UNICODE_STRING S4U2proxyTarget;
///   ULONG TransitedListSize;
///   [size_is( TransitedListSize )]
///   PRPC_UNICODE_STRING S4UTransitedServices;
/// } S4U_DELEGATION_INFO, * PS4U_DELEGATION_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct S4U_DELEGATION_INFO {
    pub S4U2proxyTarget: RPC_UNICODE_STRING,
    pub TransitedListSize: ULONG,
    pub S4UTransitedServices: Vec<RPC_UNICODE_STRING>,
}
