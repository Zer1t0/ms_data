use crate::PISID;
use ms_dtyp::ULONG;

/// typedef struct _KERB_SID_AND_ATTRIBUTES {
///   PISID Sid;
///   ULONG Attributes;
/// } KERB_SID_AND_ATTRIBUTES,
///  *PKERB_SID_AND_ATTRIBUTES;
#[derive(Clone, Default, Debug, PartialEq)]
pub struct KERB_SID_AND_ATTRIBUTES {
    pub Sid: PISID,
    pub Attributes: ULONG,
}
