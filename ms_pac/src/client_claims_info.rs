
use ms_adts::CLAIMS_SET_METADATA;


/// ```c
/// typedef struct _PAC_CLIENT_CLAIMS_INFO {
///   PCLAIMS_SET_METADATA Claims;
/// } PAC_CLIENT_CLAIMS_INFO, *PPAC_CLIENT_CLAIMS_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct PAC_CLIENT_CLAIMS_INFO {
    pub Claims: Vec<CLAIMS_SET_METADATA>,
}
