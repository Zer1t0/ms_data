use ms_dtyp::{
    UCHAR, ULONG, RPC_UNICODE_STRING
};

/// ```c
/// typedef struct _PAC_CREDENTIAL_INFO {
///   ULONG Version;
///   ULONG EncryptionType;
///   UCHAR SerializedData[1];
/// } PAC_CREDENTIAL_INFO, *PPAC_CREDENTIAL_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct PAC_CREDENTIAL_INFO {
    pub Version: ULONG,
    pub EncryptionType: ULONG,
    pub SerializedData: UCHAR,
}


/// ```c
/// typedef struct _SECPKG_SUPPLEMENTAL_CRED {
///   RPC_UNICODE_STRING PackageName;
///   ULONG CredentialSize;
///   [size_is(CredentialSize)]
///   PUCHAR Credentials;
/// } SECPKG_SUPPLEMENTAL_CRED, *PSECPKG_SUPPLEMENTAL_CRED;
/// ```
pub struct SECKPG_SUPPLEMENTAL_CRED {
    pub PackageName: RPC_UNICODE_STRING,
    pub CredentialSize: ULONG,
    pub Credentials: Vec<UCHAR>,
}

/// ```c
/// typedef struct _PAC_CREDENTIAL_DATA {
///   ULONG CredentialCount;
///   [size_is(CredentialCount)]
///   SECPKG_SUPPLEMENTAL_CRED Credentials[*];
/// } PAC_CREDENTIAL_DATA,
/// *PPAC_CREDENTIAL_DATA;
/// ```
pub struct PAC_CREDENTIAL_DATA {
    pub CredentialCount: ULONG,
    pub Credentials: Vec<SECKPG_SUPPLEMENTAL_CRED>,
}


/// ```c
/// typedef struct _NTLM_SUPPLEMENTAL_CREDENTIAL {
///   ULONG Version;
///   ULONG Flags;
///   UCHAR LmPassword[16];
///   UCHAR NtPassword[16];
/// } NTLM_SUPPLEMENTAL_CREDENTIAL, *PNTLM_SUPPLEMENTAL_CREDENTIAL;
/// ```
pub struct NTLM_SUPPLEMENTAL_CREDENTIAL {
    pub Version: ULONG,
    pub Flags: ULONG,
    pub LmPassword: [UCHAR; 16],
    pub NtPassword: [UCHAR; 16],
}
