use crate::{
    DOMAIN_GROUP_MEMBERSHIP, GROUP_MEMBERSHIP, KERB_SID_AND_ATTRIBUTES, PISID,
};
use ms_adts::CLAIMS_SET_METADATA;
use ms_dtyp::ULONG;

/// ```c
/// typedef struct _PAC_DEVICE_INFO {
///   ULONG UserId;
///   ULONG PrimaryGroupId;
///   PISID AccountDomainId;
///   ULONG AccountGroupCount;
///   [size_is(AccountGroupCount)] PGROUP_MEMBERSHIP AccountGroupIds;
///   ULONG SidCount;
///   [size_is(SidCount)] PKERB_SID_AND_ATTRIBUTES ExtraSids;
///   ULONG DomainGroupCount;
///   [size_is(DomainGroupCount)] PDOMAIN_GROUP_MEMBERSHIP DomainGroup;
/// } PAC_DEVICE_INFO, *PPAC_DEVICE_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct PAC_DEVICE_INFO {
    pub UserId: ULONG,
    pub PrimaryGroupId: ULONG,
    pub AccountDomainId: PISID,
    pub AccountGroupCount: ULONG,
    pub AccountGroupIds: Vec<GROUP_MEMBERSHIP>,
    pub SidCount: ULONG,
    pub ExtraSids: Vec<KERB_SID_AND_ATTRIBUTES>,
    pub DomainGroupCount: ULONG,
    pub DomainGroup: Vec<DOMAIN_GROUP_MEMBERSHIP>,
}

/// ```c
/// typedef struct _PAC_DEVICE_CLAIMS_INFO {
///   PCLAIMS_SET_METADATA Claims;
/// } PAC_DEVICE_CLAIMS_INFO, *PPAC_DEVICE_CLAIMS_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct PAC_DEVICE_CLAIMS_INFO {
    pub Claims: Vec<CLAIMS_SET_METADATA>,
}
