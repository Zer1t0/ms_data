use ms_dtyp::{FILETIME, WCHAR};

/// ```c
/// typedef struct _PAC_CLIENT_INFO {
///   FILETIME ClientId;
///   USHORT NameLength;
///   WCHAR Name[1];
/// } PAC_CLIENT_INFO, *PPAC_CLIENT_INFO;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct PAC_CLIENT_INFO {
    pub ClientId: FILETIME,
    pub Name: Vec<WCHAR>,
}

impl PAC_CLIENT_INFO {
    pub fn new(ClientId: FILETIME, Name: &str) -> Self {
        return Self {
            ClientId,
            Name: Name.encode_utf16().collect(),
        };
    }

    pub fn build(&self) -> Vec<u8> {
        let mut data = Vec::new();
        data.append(&mut self.ClientId.to_le_bytes().to_vec());
        data.append(&mut ((self.Name.len()*2) as u16).to_le_bytes().to_vec());

        for c in self.Name.iter() {
            data.append(&mut c.to_le_bytes().to_vec());
        }

        return data;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_build_pac_client_info() {
        let client =
            PAC_CLIENT_INFO::new(0x1d633f10ea77a00.into(), "stegosaurus");
        assert_eq!(
            vec![
                0x00, 0x7A, 0xA7, 0x0E, 0xF1, 0x33, 0xD6, 0x01, 0x16, 0x00,
                0x73, 0x00, 0x74, 0x00, 0x65, 0x00, 0x67, 0x00, 0x6F, 0x00,
                0x73, 0x00, 0x61, 0x00, 0x75, 0x00, 0x72, 0x00, 0x75, 0x00,
                0x73, 0x00
            ],
            client.build()
        );
    }
}
