use crate::byte;

use std::convert::TryFrom;

/// typedef struct _RPC_SID_IDENTIFIER_AUTHORITY {
///   byte Value[6];
/// } RPC_SID_IDENTIFIER_AUTHORITY;
pub type RPC_SID_IDENTIFIER_AUTHORITY = [byte; 6];

/// typedef struct _RPC_SID {
///   unsigned char Revision;
///   unsigned char SubAuthorityCount;
///   RPC_SID_IDENTIFIER_AUTHORITY IdentifierAuthority;
///   [size_is(SubAuthorityCount)] unsigned long SubAuthority[];
/// } RPC_SID,
///  *PRPC_S
#[derive(Clone, Default, Debug, PartialEq)]
pub struct RPC_SID {
    pub Revision: u8,
    pub IdentifierAuthority: RPC_SID_IDENTIFIER_AUTHORITY,
    pub SubAuthority: Vec<u32>,
}

impl TryFrom<&str> for RPC_SID {
    type Error = String;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let parts: Vec<&str> = s.split("-").collect();

        if parts.len() < 3 {
            return Err(format!("Invalid SID {}", s));
        }

        let revision: u8 = parts[1].parse().map_err(|_| {
            format!("Invalid sid Revision {} of {}", parts[1], s)
        })?;

        let id_auth: u8 = parts[2].parse().map_err(|_| {
            format!("Invalid sid IdentifierAuthority {} of {}", parts[2], s)
        })?;

        let mut sub_auths = Vec::new();
        for sub_auth in parts[3..].iter() {
            sub_auths.push(sub_auth.parse().map_err(|_| {
                format!("Invalid sid SubAuthority {} of {}", sub_auth, s)
            })?);
        }

        return Ok(RPC_SID {
            Revision: revision,
            IdentifierAuthority: [0, 0, 0, 0, 0, id_auth],
            SubAuthority: sub_auths,
        });
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_str_to_sid() {
        let sid_str = "S-1-5-21-1339291983-1349129144-367733775";

        assert_eq!(
            RPC_SID {
                Revision: 1,
                IdentifierAuthority: [0, 0, 0, 0, 0, 5],
                SubAuthority: vec![21, 1339291983, 1349129144, 367733775]
            },
            RPC_SID::try_from(sid_str).unwrap()
        )
    }

    #[test]
    fn test_str_to_sid_without_subs() {
        let sid_str = "S-1-5";

        assert_eq!(
            RPC_SID {
                Revision: 1,
                IdentifierAuthority: [0, 0, 0, 0, 0, 5],
                SubAuthority: vec![]
            },
            RPC_SID::try_from(sid_str).unwrap()
        )
    }
}
