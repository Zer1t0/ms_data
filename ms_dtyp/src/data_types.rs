use crate::wchar_t;

/// typedef unsigned char BYTE, *PBYTE, *LPBYTE;
pub type BYTE = u8;

/// typedef char CHAR, *PCHAR;
pub type CHAR = i8;

/// typedef unsigned long DWORD, *PDWORD, *LPDWORD;
pub type DWORD = u32;

/// typedef unsigned char UCHAR, *PUCHAR;
pub type UCHAR = u8;

/// typedef unsigned short USHORT;
pub type USHORT = u16;

/// typedef unsigned long ULONG, *PULONG;
pub type ULONG = u32;

/// typedef unsigned __int64 ULONG64;
pub type ULONG64 = u64;

/// typedef wchar_t WCHAR, *PWCHAR;
pub type WCHAR = wchar_t;
