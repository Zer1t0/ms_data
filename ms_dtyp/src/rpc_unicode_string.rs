use crate::WCHAR;
use crate::utils::u16_array_to_le_bytes;

/// ```c
/// typedef struct _RPC_UNICODE_STRING {
///   unsigned short Length;
///   unsigned short MaximumLength;
///   [size_is(MaximumLength/2), length_is(Length/2)]
///   WCHAR* Buffer;
/// } RPC_UNICODE_STRING,
///  *PRPC_UNICODE_STRING;
/// ```
#[derive(Debug, Clone, PartialEq, Default)]
pub struct RPC_UNICODE_STRING {
    pub Length: u16,
    pub MaximumLength: u16,
    pub Buffer: Vec<WCHAR>,
}

impl RPC_UNICODE_STRING {

    pub fn buffer_to_be_bytes(&self) -> Vec<u8> {
        return u16_array_to_le_bytes(&self.Buffer);
    }
    
}


impl From<&str> for RPC_UNICODE_STRING {
    fn from(s: &str) -> Self {
        let uni_str: Vec<WCHAR> = s.encode_utf16().collect();

        return Self {
            Length: uni_str.len() as u16 * 2,
            MaximumLength: uni_str.len() as u16 * 2,
            Buffer: uni_str
        }
    }
}



#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test_rpc_unicode_string_from_str() {
        let un = RPC_UNICODE_STRING::from("stegosaurus");

        assert_eq!(
            RPC_UNICODE_STRING {
                Length: 0x16,
                MaximumLength: 0x16,
                Buffer: vec![0x73, 0x74, 0x65, 0x67, 0x6f, 0x73, 0x61, 0x75, 0x72, 0x75, 0x73]
            },
            un
        );
        
    }
}   
