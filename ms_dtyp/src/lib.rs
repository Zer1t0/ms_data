//! Library to represent the data types of [MS-DTYP].
//! This library is still in very alpha version.

#![allow(non_snake_case, non_camel_case_types)]

mod base_types;
pub use base_types::{
    byte, wchar_t
};

mod data_types;
pub use data_types::{
    BYTE, CHAR, DWORD, UCHAR, USHORT, ULONG, ULONG64, WCHAR
};


mod filetime;
pub use filetime::FILETIME;

mod rpc_unicode_string;
pub use rpc_unicode_string::RPC_UNICODE_STRING;

mod sid;
pub use sid::{RPC_SID, RPC_SID_IDENTIFIER_AUTHORITY};

mod well_known_sids;
pub use well_known_sids::*;

mod utils;
