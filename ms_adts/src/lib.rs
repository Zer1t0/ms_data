//! Library to represent the data types of [MS-ADTS]
//! Still in very alpha version.

#![allow(non_snake_case, non_camel_case_types)]

pub use ms_dtyp::{
    ULONG, USHORT, BYTE
};

/// ```c
/// typedef enum _CLAIMS_COMPRESSION_FORMAT
/// {
///  COMPRESSION_FORMAT_NONE = 0,
///  COMPRESSION_FORMAT_LZNT1 = 2,
///  COMPRESSION_FORMAT_XPRESS = 3,
///  COMPRESSION_FORMAT_XPRESS_HUFF = 4
/// } CLAIMS_COMPRESSION_FORMAT;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub enum CLAIMS_COMPRESSION_FORMAT {
    COMPRESSION_FORMAT_NONE,
    COMPRESSION_FORMAT_LZNT1,
    COMPRESSION_FORMAT_XPRESS,
    COMPRESSION_FORMAT_XPRESS_HUFF,
}

/// ```c
/// typedef struct _CLAIMS_SET_METADATA {
///   ULONG ulClaimsSetSize;
///   [size_is(ulClaimsSetSize)] BYTE* ClaimsSet;
///   CLAIMS_COMPRESSION_FORMAT usCompressionFormat;
///   ULONG ulUncompressedClaimsSetSize;
///   USHORT usReservedType;
///   ULONG ulReservedFieldSize;
/// [size_is(ulReservedFieldSize)] BYTE* ReservedField;
/// } CLAIMS_SET_METADATA,
///  *PCLAIMS_SET_METADATA;
/// ```
#[derive(Clone, Debug, PartialEq)]
pub struct CLAIMS_SET_METADATA {
    pub ulClaimsSetSize: ULONG,
    pub ClaimSet: Vec<BYTE>,
    pub usCompressionFormat: CLAIMS_COMPRESSION_FORMAT,
    pub ulUncompressedClaimsSetSize: ULONG,
    pub usReservedType: USHORT,
    pub ulReservedFieldSize: ULONG,
    pub ReservedField: Vec<BYTE>,
}

